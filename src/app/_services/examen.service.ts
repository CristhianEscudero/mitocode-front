import { Injectable } from '@angular/core';
import { Examen } from './../_model/examen';
import { environment } from './../../environments/environment';
import { GenericService } from './generic.service';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExamenService extends GenericService<Examen>{

  private examenCambio = new Subject<Examen[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {
    super(
      http,
      `${environment.HOST}/examen`);
  }

  getExamenCambio() {
    return this.examenCambio.asObservable();
  }
  
  setExamenCambio(examenes: Examen[]) {
    this.examenCambio.next(examenes);
  }
  
  getMensajeCambio() {
    return this.mensajeCambio.asObservable();
  }

  setMensajeCambio(mensaje: string) {
    this.mensajeCambio.next(mensaje);
  }
}
