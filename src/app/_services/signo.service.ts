import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Signo } from './../_model/signo';
import { Subject } from 'rxjs';
import { GenericService } from './generic.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignoService extends GenericService<Signo> {

  private signoCambio = new Subject<Signo[]>()
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) { 
    super(
      http,
      `${environment.HOST}/signos`
    )
  }

  getSignoCambio() {
    return this.signoCambio.asObservable()
  }

  setSignoCambio(signos: Signo[]) {
    this.signoCambio.next(signos)
  }

  getMensajeCambio() {
    return this.mensajeCambio.asObservable()
  }

  setMensajeCambio(mensaje: string) {
    return this.mensajeCambio.next(mensaje)
  }


}
