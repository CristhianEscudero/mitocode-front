import { ConsultaService } from './../../_services/consulta.service';
import { ConsultaListaExamenDTO } from './../../_dto/consultaListaExamenDTO';
import { Consulta } from './../../_model/consulta';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExamenService } from './../../_services/examen.service';
import { DetalleConsulta } from './../../_model/detalleConsulta';
import { Especialidad } from './../../_model/especialidad';
import { EspecialidadService } from './../../_services/especialidad.service';
import { MedicoService } from './../../_services/medico.service';
import { Medico } from './../../_model/medico';
import { PacienteService } from './../../_services/paciente.service';
import { Paciente } from './../../_model/paciente';
import { Component, OnInit } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { Examen } from 'src/app/_model/examen';
import * as moment from 'moment';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  pacientes: Paciente[]
  pacientes$: Observable<Paciente[]>
  medico$: Observable<Medico[]>
  especialidades$: Observable<Especialidad[]>
  examenes$: Observable<Examen[]>
  idPacienteSeleccionado: number
  idMedicoSeleccionado: number
  idEspecialidadSeleccionado: number
  idExamenSeleccionado: number
  maxFecha: Date = new Date()
  fechaSeleccionada: Date = new Date()
  diagnostico: string
  tratamiento: string
  detalleConsulta: DetalleConsulta[] = [];
  examenesSeleccionados: Examen[] = [];

  constructor(private pacienteService: PacienteService, private medicoService: MedicoService, private especialidadService: EspecialidadService, private examenService: ExamenService, private snackBar: MatSnackBar, private consultaService: ConsultaService) { }

  ngOnInit(): void {
    this.pacientes$ = this.pacienteService.listar()
    this.medico$ = this.medicoService.listar()
    this.especialidades$ = this.especialidadService.listar()
    this.examenes$ = this.examenService.listar()
    //this.especialidad$ = EMPTY
  }


  cambieFecha(e: any) {
    console.log(e)
  }

  agregar() {
    if (this.diagnostico != null && this.tratamiento != null) {
      let det = new DetalleConsulta()
      det.diagnostico = this.diagnostico
      det.tratamiento = this.tratamiento
      this.detalleConsulta.push(det)
      this.diagnostico = null
      this.tratamiento = null
    }
  }

  removerDiagnostico(index: number) {
    this.detalleConsulta.splice(index, 1)
  }

  agregarExamen() {
    if (this.idExamenSeleccionado > 0) {

      let cont = 0;
      for (let i = 0; i < this.examenesSeleccionados.length; i++) {
        let examen = this.examenesSeleccionados[i];
        if (examen.idExamen === this.idExamenSeleccionado) {
          cont++;
          break;
        }
      }

      if (cont > 0) {
        let mensaje = 'El examen se encuentra en la lista';
        this.snackBar.open(mensaje, "Aviso", { duration: 2000 });
      } else {
        this.examenService.listarPorId(this.idExamenSeleccionado).subscribe(data => {
          this.examenesSeleccionados.push(data);
        });

      }
    }
  }

  removerExamen(index: number) {
    this.examenesSeleccionados.splice(index, 1)
  }

  estadoBotonRegistrar() {
    return (this.detalleConsulta.length === 0 || this.idEspecialidadSeleccionado === 0 || this.idMedicoSeleccionado === 0 || this.idPacienteSeleccionado === 0);
  }

  aceptar() {
    let medico = new Medico()
    medico.idMedico = this.idMedicoSeleccionado

    let especialidad = new Especialidad()
    especialidad.idEspecialidad = this.idEspecialidadSeleccionado

    let paciente = new Paciente()
    paciente.idPaciente = this.idPacienteSeleccionado

    let consulta = new Consulta()
    consulta.especialidad = especialidad
    consulta.medico = medico
    consulta.paciente = paciente
    consulta.numConsultorio = "C1"

    consulta.fecha = moment(this.fechaSeleccionada).format('YYYY-MM-DDTHH:mm:ss');
    consulta.detalleConsulta = this.detalleConsulta;

    let consultaListaExamenDTO = new ConsultaListaExamenDTO();
    consultaListaExamenDTO.consulta = consulta;
    consultaListaExamenDTO.lstExamen = this.examenesSeleccionados;

    this.consultaService.registrarTransaccion(consultaListaExamenDTO).subscribe(() => {
      this.snackBar.open("Se registró", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.limpiarControles();
      }, 2000);
    });
  }

  limpiarControles() {
    this.detalleConsulta = [];
    this.examenesSeleccionados = [];
    this.diagnostico = null;
    this.tratamiento = null;
    this.idPacienteSeleccionado = 0;
    this.idEspecialidadSeleccionado = 0;
    this.idMedicoSeleccionado = 0;
    this.idExamenSeleccionado = 0;
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
  }

}

