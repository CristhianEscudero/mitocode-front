import { MatSnackBar } from '@angular/material/snack-bar';
import { Signo } from './../../../_model/signo';
import { map, switchMap } from 'rxjs/operators';
import { PacienteService } from './../../../_services/paciente.service';
import { Paciente } from './../../../_model/paciente';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { SignoService } from './../../../_services/signo.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  form: FormGroup;
  id: number;
  edicion: boolean

  pacientes: Paciente[] = []
  pacienteSeleccionado: Paciente
  myControlPaciente: FormControl = new FormControl()
  pacientesFiltrados$: Observable<Paciente[]>;

  temperatura: string;
  pulso: string;
  ritmo: string;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  constructor(
    private signoService: SignoService,
    private pacienteService: PacienteService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl('')
    })
    this.listarPacientes();
    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)))
    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null
      this.initForm();
    })

  }

  filtrarPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      )
    }
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    )
  }

  private initForm() {
    if (this.edicion) { }
    this.signoService.listarPorId(this.id).subscribe(data => {
      this.form = new FormGroup({
        'id': new FormControl(data.idSigno),
        'paciente': new FormControl(data.paciente),
        'fecha': new FormControl(moment(data.fecha).format('YYYY-MM-DDTHH:mm:ss')),
        'temperatura': new FormControl(data.temperatura),
        'pulso': new FormControl(data.pulso),
        'ritmo': new FormControl(data.ritmoRespiratorio)
      });
    })
  }

  get f() {
    return this.form.controls
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data
    })
  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value
  }

  /*   agregar(){
      let signo = new Signo();
      signo.paciente = this.form.value['paciente']
      signo.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss')
      signo.temperatura = this.form.value['temperatura']
      signo.pulso = this.form.value['pulso']
      signo.ritmoRespiratorio = this.form.value['ritmo']
  
      this.signoService.registrar(signo).subscribe(data=>{
        this.signoService.listar()
        console.log(data)
      })
      this.snackBar.open("Se registró", "Aviso", { duration: 2000 })
      this.limpiarControles()
      this.router.navigate(['signo'])
    }
   */

  operar() {

    let signo = new Signo();
    signo.idSigno = this.form.value['id']
    signo.paciente = this.form.value['paciente']
    signo.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss')
    signo.temperatura = this.form.value['temperatura']
    signo.pulso = this.form.value['pulso']
    signo.ritmoRespiratorio = this.form.value['ritmo']

    if (this.edicion) {
      this.signoService.modificar(signo).pipe(switchMap(() => {
        return this.signoService.listar()
      })).subscribe(data => {
        this.signoService.setSignoCambio(data)
        this.signoService.setMensajeCambio("SE MODIFICO")
        this.router.navigate(['signo'])
      })

    } else {
      this.signoService.registrar(signo).pipe(switchMap(() => {
        return this.signoService.listar()
      })).subscribe(data => {
        this.signoService.setSignoCambio(data)
        this.signoService.setMensajeCambio("SE REGISTRO")
        this.router.navigate(['signo'])
      })
    }

/*     if (this.edicion) {
      this.signoService.modificar(signo).subscribe(data => {
        this.signoService.listar()
        console.log(data)
        this.snackBar.open("Se modifico", "Aviso", { duration: 2000 })
        this.limpiarControles()
        this.router.navigate(['signo'])
      })
    } else {
      this.signoService.registrar(signo).subscribe(data =>{
        this.signoService.listar()
        console.log(data)
        this.snackBar.open("Se registró", "Aviso", { duration: 2000 })
        this.limpiarControles()
        this.router.navigate(['signo'])
      })
    }
 */
    
  }

  limpiarControles() {
    this.temperatura = ''
    this.pulso = ''
    this.ritmo = ''
    this.fechaSeleccionada = new Date()
    this.fechaSeleccionada.setHours(0)
    this.fechaSeleccionada.setMinutes(0)
    this.fechaSeleccionada.setSeconds(0)
    this.fechaSeleccionada.setMilliseconds(0)
    this.myControlPaciente.reset()
  }

}
