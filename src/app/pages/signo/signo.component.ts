import { switchMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignoService } from './../../_services/signo.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Signo } from './../../_model/signo';
import { MatTableDataSource } from '@angular/material/table';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-signo',
  templateUrl: './signo.component.html',
  styleUrls: ['./signo.component.css']
})

export class SignoComponent implements OnInit {

  columnasAMostrar = ['idSigno', 'Paciente', 'Fecha', 'Temperatura', 'Pulso', 'Ritmo', 'Acciones']
  dataSource: MatTableDataSource<Signo>
  @ViewChild(MatSort) sort: MatSort
  @ViewChild(MatPaginator) paginator: MatPaginator

  constructor(
    private signoService: SignoService,
    private snackBar: MatSnackBar
    ) { }

    ngOnInit(): void {
      this.signoService.getSignoCambio().subscribe(data => {
        this.dataSource = new MatTableDataSource(data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
        this.signoService.listar()
      })
  
      this.signoService.getMensajeCambio().subscribe(data => {
        this.snackBar.open(data, 'AVISO', { duration: 2000 })
      })
      this.signoService.listar().subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      })

    }

  filtrar(valor: any) {
    // busca en todos los campos menos paciente porque el campo en si es su id
    this.dataSource.filter = valor.trim().toLowerCase()
    // apoya para la busqueda del paciente por nombre ya que es la acción adecuada
    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr =JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) != -1; 
    }
  }

  eliminar(idSigno: number){
    this.signoService.eliminar(idSigno).pipe(switchMap( ()=> {
      return this.signoService.listar()
    })).subscribe( data => {
      this.signoService.setSignoCambio(data)
      this.signoService.setMensajeCambio('SE ELIMINO')
    })
  }
}
