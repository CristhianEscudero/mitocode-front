import { Menu } from './_model/menu';
import { LoginService } from './_services/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'mediapp';

  menus: Menu[]

  constructor(
    public loginService: LoginService
  ){}

ngOnInit(){
  this.loginService.getMenuCambio().subscribe(data=>{
    this.menus=data
  })
}

}
